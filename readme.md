<h1>CCPI</h1>
This is my Control Center for my Private RaspberryPI at home.<br>
Feel free to have a look into my world.<br>
<br>
<h3>What does it do?</h3>
I installed two relais to simulate a power and reset button to start a PC. (Magic-packet is not aviable)<br>
<br>
<h3>Why this project?</h3>
If my SD-card crashes i have a backup documentation.<br>
<br>
<h3>Why public?</h3>
i get so much help in the web so other people can have a look to lern.<br>
<br>


<br>
<br>
<h2>### INSTALL CCPI ###</h2>
<br>
System Update:
```	
sudo apt-get update
sudo apt-get upgrade
sudo apt-get -y dist-upgrade
sudo apt-get -y autoremove
sudo apt-get -y autoclean
```
<br>

Firmware Update:
```
sudo rpi-update
```
<br>

move to home directory:
```
cd /home/pi
```
<br>
Install git (if needed):
```
sudo apt-get install git-all
```
<br>
Clone ccpi:
```
git@gitlab.com:vreezy117/ccpi.git
git clone
```
<br>
Install pip and flask for python (if needed):
```
sudo apt-get install python-pip
sudo pip install Flask
```
<br>
Start server:
```
python webserver.py
```
<br>

<h3>Server Autostart</h3>
open rc.local:<br>
```
sudo nano /etc/rc.local
```
<br>
add bevor "exit 0":<br>
```
python /home/pi/webserver.py &
```
<br>
Save and Close.<br>
STRG+O, STRG+X.<br>
<br>
<br>
Restart PI to test:
```
sudo reboot
```
<br>

<h3>Hints</h3>
* the Webserver runs on port 5000! edit in webserver.py the last line to change the port.<br>
* all files in your /home/pi directory are aviable over http!<br>
<br>


<br>
<br>
<h2>### OPENVPN CONFIG ###</h2>
<br>
<h3>Windows Client Autostart</h3>
press: WIN + R and enter:<br>
```
shell:startup
```
right click in the folder, create shortcut:<br>
```
"C:\Program Files\OpenVPN\bin\openvpn-gui.exe" --config_dir "D:\openvpn" --connect client-config.ovpn
```
<br>


<br>
<br>
<h2>### Licence ###</h2>
MIT License

Copyright (c) 2018 Lars 'vreezy' Eschweiler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.