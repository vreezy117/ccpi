$(document).ready(function(){

    $.ajax({
        url: '/api/status',
        type: 'GET',
        beforeSend: function() {
            $("#status").html("Trying connection to API...");

        },
        success: function (result) {
            var status = result.status;
            var msg = result.msg;
            var power = result.power;
            var reset = result.reset;
            
            if(status == "ok") {
                $("#status").html(msg);
            }
            else {
                $("#status").html("There was an ERROR! look in the console.");
                console.log(result);
            }

        }, 
        error: function(e) {
            console.log(e);
            $("#p1_submit").html('Error');
        } 
    });




	$('#power_1').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/power/1',
            type: 'GET',
            beforeSend: function() {
                
                $('#p1_submit').prop('disabled', true);
                $("#p1_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#p1_submit').prop('disabled', false);
                    $("#p1_submit").html('Power 1sec');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#p1_submit").html('Error');
            } 
        });
    });

    $('#power_5').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/power/5',
            type: 'GET',
            beforeSend: function() {
                
                $('#p5_submit').prop('disabled', true);
                $("#p5_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#p5_submit').prop('disabled', false);
                    $("#p5_submit").html('Power 5sec');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#p5_submit").html('Error');
            } 
        });
    });

    $('#power_on').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/power/on',
            type: 'GET',
            beforeSend: function() {
                
                $('#pon_submit').prop('disabled', true);
                $("#pon_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#pon_submit').prop('disabled', false);
                    $("#pon_submit").html('Power keep on');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#pon_submit").html('Error');
            } 
        });
    });

    $('#power_off').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/power/off',
            type: 'GET',
            beforeSend: function() {
                
                $('#poff_submit').prop('disabled', true);
                $("#poff_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#poff_submit').prop('disabled', false);
                    $("#poff_submit").html('Power off');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#poff_submit").html('Error');
            } 
        });
    });








    $('#reset_1').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/reset/1',
            type: 'GET',
            beforeSend: function() {
                
                $('#r1_submit').prop('disabled', true);
                $("#r1_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#r1_submit').prop('disabled', false);
                    $("#r1_submit").html('Reset 1sec');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#r1_submit").html('Error');
            } 
        });
    });

    $('#reset_5').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/reset/5',
            type: 'GET',
            beforeSend: function() {
                
                $('#r5_submit').prop('disabled', true);
                $("#r5_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#r5_submit').prop('disabled', false);
                    $("#r5_submit").html('Reset 5sec');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#r5_submit").html('Error');
            } 
        });
    });

    $('#reset_on').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/reset/on',
            type: 'GET',
            beforeSend: function() {
                
                $('#ron_submit').prop('disabled', true);
                $("#ron_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#ron_submit').prop('disabled', false);
                    $("#ron_submit").html('Reset keep on');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#ron_submit").html('Error');
            } 
        });
    });

    $('#reset_off').on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '/api/reset/off',
            type: 'GET',
            beforeSend: function() {
                
                $('#roff_submit').prop('disabled', true);
                $("#roff_submit").html('trying...');

            },
            success: function (result) {
                var status = result.status;
                var msg = result.msg;
                
                if(status == "ok") {
                    $("#status").html(msg);
                    $('#roff_submit').prop('disabled', false);
                    $("#roff_submit").html('Reset off');
                }
                else {
                    $("#status").html("There was an ERROR! look in the console.");
                    console.log(result);
                }

            }, 
            error: function(e) {
                console.log(e);
                $("#roff_submit").html('Error');
            } 
        });
    });




});