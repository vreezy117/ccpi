from flask import Flask, request, send_from_directory
from flask import jsonify
import RPi.GPIO as GPIO
import time

LED = 24
RESET = 16
POWER = 20
STATUSQ = 21
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(LED, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(RESET, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(POWER, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(STATUSQ, GPIO.OUT, initial=GPIO.HIGH)

app = Flask(__name__, static_url_path='', static_folder='',)

# --- HTML ---
@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('css', path)

@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('img', path)

# --- API STATUS ---
@app.route('/api/status')
def status():
    return jsonify(status="ok", msg='API is ready to use.', power=GPIO.input(POWER), reset=GPIO.input(RESET))

# --- API TEST ---
@app.route('/api/test/1')
def test_1():
    return jsonify(status="ok", msg="Test pressed for 1 sec.")

@app.route('/api/test/5')
def test_5():
    return jsonify(status="ok", msg="Test pressed for 5 sec.")

@app.route('/api/test/on')
def test_on():
    return jsonify(status="ok", msg="Test keep on.")

@app.route('/api/test/off')
def test_off():
    return jsonify(status="ok", msg="Test off.")

# --- API ---
@app.route('/api/power/1')
def power_1():
    GPIO.output(POWER,GPIO.LOW)
    time.sleep(1)
    GPIO.output(POWER,GPIO.HIGH)
    return jsonify(status="ok", power=GPIO.input(POWER), msg="Power pressed for 1 sec.")

@app.route('/api/power/5')
def power_5():
    GPIO.output(POWER,GPIO.LOW)
    time.sleep(5)
    GPIO.output(POWER,GPIO.HIGH)
    return jsonify(status="ok", power=GPIO.input(POWER), msg="Power pressed for 5 sec.")

@app.route('/api/reset/1')
def reset_1():
    GPIO.output(RESET,GPIO.LOW)
    time.sleep(1)
    GPIO.output(RESET,GPIO.HIGH)
    return jsonify(status="ok", reset=GPIO.input(RESET), msg="Reset pressed for 1 sec.")

@app.route('/api/reset/5')
def reset_5():
    GPIO.output(RESET,GPIO.LOW)
    time.sleep(5)
    GPIO.output(RESET,GPIO.HIGH)
    return jsonify(status="ok", reset=GPIO.input(RESET), msg="Reset pressed for 5 sec.")

@app.route('/api/power/on')
def power_on():
    GPIO.output(POWER,GPIO.LOW)
    return jsonify(status="ok", power=GPIO.input(POWER), msg="Power keep on.")

@app.route('/api/power/off')
def power_off():
    GPIO.output(POWER,GPIO.HIGH)
    return jsonify(status="ok", power=GPIO.input(POWER), msg="Power off.")

@app.route('/api/reset/on')
def reset_on():
    GPIO.output(RESET,GPIO.LOW)
    return jsonify(status="ok", reset=GPIO.input(RESET), msg="Reset keep on.")

@app.route('/api/reset/off')
def reset_off():
    GPIO.output(RESET,GPIO.HIGH)
    return jsonify(status="ok", reset=GPIO.input(RESET), msg="Reset off.")

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0")
    #app.run(host="0.0.0.0", port=80)